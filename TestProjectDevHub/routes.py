from views import login, add_node, search_text, sub_tree


def setup_routes(app):
    app.add_route('POST', '/login', login)
    app.add_route('POST', '/add_node', add_node)
    app.add_route('GET', '/search_text', search_text)
    app.add_route('GET', '/sub_tree', sub_tree)
