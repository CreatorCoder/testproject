from os.path import abspath, join, dirname
import yaml


# Load config file
CONFIG_FILE = abspath(join(dirname(__file__), 'config.yaml'))
app_config = None
with open(CONFIG_FILE, 'r') as config:
    app_config = yaml.load(config)


MONGODB_HOST = app_config['storage_tree']['mongodb']['host']
MONGODB_PORT = app_config['storage_tree']['mongodb']['port']

SERVICE_HOST = app_config['service_api']['host']
SERVICE_PORT = app_config['service_api']['port']


JWT_SECRET = 'secret'
JWT_ALGORITHM = 'HS256'
JWT_EXP_DELTA_SECONDS = 6000
