from datetime import timedelta, datetime
import jwt
from aiohttp.web_response import json_response
import settings
import json

# Игрушечная база данных
from middleware import login_required
from storage_tree.stogage_mongo import StorageMongo

db_users = [{'user_id': 100, 'user_name': 'kolay', 'password': 'qwerty'}]

storage_mongo = StorageMongo()


class DoesNotExist(Exception):
    pass


class PasswordDoesNotMatch(Exception):
    pass


async def login(request):
    """
    Sign up
    ---
    summary: Login and password
    tags:
      - login
    responses:
      '200':
        description: Autorisation is good
        content:
          application/json:
              schema:
                  $ref: "#/components/schemas/token"
    """
    post_data = await request.json()
    try:
        user_name = post_data['user_name']
        user_password = post_data['password']
        # TODO реализовать полноценную базу данных с учетными записями пользователей
        user_id = None
        for db_user in db_users:
            if db_user['user_name'] == user_name:
                if db_user['password'] != user_password:
                    raise PasswordDoesNotMatch
                user_id = db_user['user_id']
        else:
            if not user_id:
                raise DoesNotExist
    except (DoesNotExist, PasswordDoesNotMatch):
        return json_response({'message': 'Wrong credentials'}, status=400)

    payload = {
        'user_id': user_id,  # user_id извлекается из базы данных по логину и паролю
        'exp': datetime.utcnow() + timedelta(seconds=settings.JWT_EXP_DELTA_SECONDS)
    }
    jwt_token = jwt.encode(payload, settings.JWT_SECRET, settings.JWT_ALGORITHM)
    return json_response({'token': jwt_token.decode('utf-8')})


@login_required
async def add_node(request):
    """
    Add new node
    ---
    summary: Adding nodes
    tags:
      - Nodes
    responses:
      '200':
        description: The Node is added
        content:
          application/json:
              schema:
                  $ref: "#/components/schemas/result"
    """
    post_data = await request.json()
    try:
        text_node = post_data['text_node']
        parent_node_id = post_data['parent_node_id'] if 'parent_node_id' in post_data else None
        if parent_node_id:
            storage_mongo.add_new_node(text_node=text_node, parent_node=storage_mongo.get_node_by_id(parent_node_id))
        else:
            storage_mongo.add_new_node(text_node=text_node)
        return json_response(text='Well done!', status=200)
    except (DoesNotExist, PasswordDoesNotMatch):
        return json_response({'message': 'Error add new node'}, status=400)


@login_required
async def search_text(request):
    """
    Full text search
    ---
    summary: Text searching
    tags:
      - text
    responses:
      '200':
        description: The pattern text is found
        content:
          application/json:
              schema:
                  $ref: "#/components/schemas/result"
    """
    get_data = await request.json()
    output = []
    for ones in storage_mongo.search(get_data['pattern']):
        output.append(str(ones['_id']))
    return json_response({'found': json.dumps(output)}, status=200)


@login_required
async def sub_tree(request):
    """
    Get sub tree
    ---
    summary: Sub tree by node id
    tags:
      - sub tree
    responses:
      '200':
        description: Sub tree ia found
        content:
          application/json:
              schema:
                  $ref: "#/components/schemas/result"
    """
    get_data = await request.json()
    node_id = get_data['node_id']
    sub_tree_nodes = storage_mongo.mongo_tree.get_sub_tree(node_id)
    return json_response({'found': json.dumps(sub_tree_nodes)}, status=200)

