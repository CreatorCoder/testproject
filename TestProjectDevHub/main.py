#!/usr/bin/python
# -*- coding: utf-8 -*-

from aiohttp import web
from aiohttp_swagger3 import SwaggerDocs
from routes import setup_routes
import settings
from middleware import auth_middleware


app = web.Application(middlewares=[auth_middleware])

swag_doc = SwaggerDocs(app, '/docs', title="REST API Documantation", version="1.0.0", components="components.yaml")  # components="components.yaml"

setup_routes(swag_doc)

web.run_app(app, host=settings.SERVICE_HOST, port=settings.SERVICE_PORT)

