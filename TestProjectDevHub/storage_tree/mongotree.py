from bson import ObjectId


class NodeNotFound(Exception):
    """Node not found."""
    pass


class MismatchedNodeIdentifier(Exception):
    """Node identifier mismatch from an update."""
    pass


class MongoTreeNode(object):
    """
    A node in a tree.
    """
    def __init__(self, content={}):
        self.parent_id = None
        self.children = []
        self._id = ObjectId()
        self.text_node = ''
        if content:
            self.load_from_dict(content)

    def load_from_dict(self, content):
        for k, v in content.items():
            self.__setattr__(k, v)

    def __len__(self):
        return len(self.children)

    def __iter__(self):
        return iter(self.children)

    def append(self, node):
        self.children.append(node)

    def to_dict(self):
        some_dict = {}
        for attr in dir(self):
            some_dict[attr] = self.__getattribute__(attr)
        return some_dict

    def __dir__(self):
        return {key: value for key, value in self.__dict__.items() if not key.startswith("__")}


class MongoTree(object):
    def __init__(self, collection):
        self.collection = collection
        content_node = self.collection.find_one({'parent_id': None})
        if content_node is None:
            self.root_node = MongoTreeNode(content=content_node)
            self.insert_node(self.root_node)

    def insert_node(self, node):
        some_dict = node.to_dict()
        self.collection.insert(some_dict)

    def save_node(self, node):
        some_dict = node.to_dict()

        stats = self.collection.find_and_modify(
            query={"_id": node._id},
            update=some_dict,
            fields={"_id": 1},
            )
        if not stats:
            raise NodeNotFound(str(node._id))
        if stats['_id'] != node._id:
            raise MismatchedNodeIdentifier(str(node._id))

    def save_all_nodes(self):
        pass

    def get_node(self, node_id):
        # find the object
        node_id = node_id if isinstance(node_id, ObjectId) else ObjectId(node_id)
        some_dict = self.collection.find_one({"_id": node_id})
        if not some_dict:
            raise NodeNotFound(str(node_id))

        # turn it into a (detached) node
        some_node = MongoTreeNode(content=some_dict)

        return some_node

    def del_node(self, node_id):
        # remove the document
        stats = self.collection.remove(node_id)
        if not stats or stats['n'] != 1:
            raise NodeNotFound(str(node_id))

    def get_sub_tree(self, node_id, sub_tree=[]):
        for child_id in self.get_node(node_id).children:
            sub_tree.append(str(child_id))
            self.get_sub_tree(child_id, sub_tree)
        return sub_tree
