import pymongo
from pymongo import MongoClient
import settings
from storage_tree.mongotree import MongoTree, MongoTreeNode


class StorageMongo:
    def __init__(self):
        self.client = MongoClient(settings.MONGODB_HOST, settings.MONGODB_PORT)
        self.db = self.client['my_tree']
        self.nodes = self.db['nodes']

        if 'text_node_text' not in self.nodes.index_information():
            self.nodes.create_index([('text_node', pymongo.TEXT)])  # for full text search

        self.mongo_tree = MongoTree(self.nodes)

    def add_new_node(self, text_node='', parent_node=None):
        new_node = MongoTreeNode()
        new_node.text_node = text_node
        if parent_node is not None:
            new_node.parent_id = parent_node._id
            parent_node.children.append(new_node._id)
            self.mongo_tree.save_node(parent_node)
        else:
            new_node.parent_id = self.mongo_tree.root_node._id
            self.mongo_tree.root_node.children.append(new_node._id)
            self.mongo_tree.save_node(self.mongo_tree.root_node)
        self.mongo_tree.insert_node(new_node)

    def search(self, pattern):
        results = self.nodes.find({'$text': {'$search': pattern}})
        return list(results)

    def get_node_by_id(self, node_id):
        return self.mongo_tree.get_node(node_id)
